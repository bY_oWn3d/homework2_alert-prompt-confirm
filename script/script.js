// Теоретичні питання

//1.Які існують типи даних у Javascript?

// Типи данних 8 (Примітивні типи - String, Number, Boolean, Null, Undefined, Big Int, Symbol), окремий тип Object

//String
const greetings = "Hello World";
console.log(typeof greetings);

//Number
const num = 100
console.log(typeof num);

//Boolean
const boolTrue = true;
const boolFalse = false;
console.log(typeof boolTrue);

//Null
const someNull = null;
console.log(someNull);

//Undefined
const someUndefined = undefined;
console.log(someUndefined);

//Big Int
const bigInt = 99999999999999999999n;
console.log(typeof bigInt);
console.log(bigInt);

//Symbol
const user = {
    id: 25,
    age: 30,
    height: 190,
};
const id = Symbol("New Symbol");
user[id] = 45;
console.log(typeof id);
//console.log(user); Якщо написати console.log(user[id]); Буде відображатися нове значення id aле не перезапишеться оригінальна властивість id 

//Object
//Перший спосіб запису
const obj = {
    name: "Valentyn",
    age: 30,
    hobby: "IT Technology",
    gender: "male",
};
console.log(typeof obj);
//console.log(obj); Щоб достукатись до конкретного об'єкта вводимо console.log(obj.name)

//Другий сосіб запису
const obj2 = {}
obj2.name = "Valentyn";
obj2.age = 30;
obj2.hobby = "IT Technology";
obj2.gender = "male";
console.log(typeof obj2);
//console.log(obj2); Щоб достукатись до конкретного об'єкта вводимо console.log(obj2.name)

//Третій спосіб запису
const obj3 = {}
obj3["name"] = "Valentyn";
obj3["age"] = 30;
obj3["hobby"] = "IT Technology";
obj3["gender"] = "male";
console.log(typeof obj3);
//console.log(obj3); Щоб достукатись до конкретного об'єкта вводимо console.log(obj3.name)

//2.У чому різниця між == і ===?

//Оператор == порівнює на рівність, тоді як ===  на ідентичність. Плюс оператора === полягає в тому, що він не наводить два значення одного типу. Саме тому він зазвичай і використовується

//Рівність
console.log(1 == 1);       //true
console.log(1 == "1");     //true
console.log(1 == "1a");        //false
console.log(1 == true);        //true

//Ідентичність
console.log(1 === 1);       //true
console.log(1 === "1");     //false     
console.log(1 === "1a");        //false
console.log(1 === true);        //false

//3.Що таке оператор?

//Оператор – це внутрішня функція JavaScript. При використанні того чи іншого оператора ми, по суті, просто запускаємо ту чи іншу вбудовану функцію, яка виконує певні дії і повертає результат.
//JavaScript підтримує бінарні та унарні оператори, а також ще один спеціальний тернарний оператор - умовний оператор.

//У унарних операторів завжди один операнд
let a = 1;
a = -a;
console.log( a );; // -1, застосував унарний мінус

//У бінарних операторів на відміну від унарних завжди два операнди
let b = 1, c = 3;
console.log(c - b);; // 2, бінарний мінус віднімає значення

//Завдання

// Ім'я користувача
let firstName = prompt ("Введіть ваше ім'я!")?.trim();
while (!isNaN(firstName)) {
    firstName = prompt ("Ваше ім'я введено некоректно, спробуйте ще раз");
}

//Вік користувача
let age;
if (firstName !== undefined) {
    age = +prompt ("Укажиіть ваш вік");
while (isNaN(age)) {
    age = +prompt ("Ваш вік введено некоректно, спробуйте ще раз");
}
}; 

//Перевірка за віком
if (age < 18 || firstName === undefined) {
    alert ("You are not allowed to visit this website");
} else if (age <= 22) {
    let inform = confirm ("Are you sure you want to continue?");
    if (inform) {
        alert (`Welcome, ${firstName}`);
    } else {
        alert ("You are not allowed to visit this website");
    }
} else {
    alert (`Welcome, ${firstName}`);
}